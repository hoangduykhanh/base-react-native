import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { ExampleDetailScreen } from "../screens/example-detail-screen";
import { ExampleScreen } from "../screens/example-screen";
import { EXAMPLE_SCREEN, EXAMPLE_SCREEN_DETAIL } from "./constant";

const Stack = createNativeStackNavigator();

export default function MyNavigationContainer() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={EXAMPLE_SCREEN}>
        <Stack.Screen name={EXAMPLE_SCREEN} component={ExampleScreen} />
        <Stack.Screen
          name={EXAMPLE_SCREEN_DETAIL}
          component={ExampleDetailScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
