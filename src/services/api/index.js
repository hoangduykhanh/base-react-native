import axios from "axios";
import { validateStatus } from "../../utils/validate-status";

const BASE_URL = process.env.REACT_APP_HOST + "/api";

export const APIEnum = {
  REGISTER: "/auth/register",
  LOGIN: "/auth/login",
  PROFILE: "/auth/profile",
  GET_TOKEN: "/auth/token",
  BRANCH: "/branch",
  CATEGORY: "/category",
  COLOR: "/color",
  COUPON: "/coupon",
  STORAGE: "/storage",
  SPECIFICATION: "/specification",
  SETTING: "/setting",
  PRODUCT: "/product",
  USER: "/user",
  DASHBOARD: "/dashboard",
  ORDER: "/order",
};

const HEADERS_MULTIPLE_PART = {
  "Content-Type": "multipart/form-data; boundary=something",
};

export const createInstance = (baseURL) => {
  const instance = axios.create({
    baseURL: baseURL,
    headers: {
      contentType: "application/json",
      accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

  // Add a request interceptor
  instance.interceptors.request.use(
    async function (config) {
      const token = await AsyncStorage.getItem("token");
      if (config.url !== APIEnum.GET_TOKEN && token) {
        config.headers["Authorization"] = `Bearer ${token}`;
      }
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );

  // Add a response interceptor
  instance.interceptors.response.use(
    async function (response) {
      if (validateStatus(response?.status)) {
        return response.data;
      } else if (response?.status === 500) {
        // show alert error
      } else {
        // show alert unauthorized
      }
    },
    async function (error) {
      const { config } = error;

      const urlIgnore = [APIEnum.LOGIN, APIEnum.REGISTER, APIEnum.GET_TOKEN];

      const refreshToken = await AsyncStorage.getItem("refreshToken");

      if (
        error?.response?.status === 401 &&
        refreshToken &&
        !urlIgnore.includes(config.url) &&
        (config.retry || 0) < 4
      ) {
        config.retry = config.retry ? config.retry + 1 : 1;
        const response = await instance.post(APIEnum.GET_TOKEN, {
          refreshToken,
        });

        const { token } = response.data;

        if (token) {
          setAuthToken(token);

          await AsyncStorage.setItem("token", token);

          return instance(config);
        }
      }
      return Promise.reject(error);
    }
  );

  return instance;
};

export const createApi = (instance) => ({
  post: (endpoint, params) => {
    return instance
      .post(endpoint, params, {
        validateStatus: (status) => validateStatus(status),
      })
      .then(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      )
      .catch(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      );
  },

  postMultiplePart: (endpoint, params) => {
    return instance
      .post(endpoint, params, {
        headers: HEADERS_MULTIPLE_PART,
        validateStatus: (status) => validateStatus(status),
      })
      .then(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      )
      .catch(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      );
  },

  putMultiplePart: (endpoint, params) => {
    return instance
      .put(endpoint, params, {
        headers: HEADERS_MULTIPLE_PART,
        validateStatus: (status) => validateStatus(status),
      })
      .then(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      )
      .catch(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      );
  },

  get: (endpoint, params = {}) => {
    return instance
      .get(endpoint, {
        params: params,
        validateStatus: (status) => validateStatus(status),
      })
      .then(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      )
      .catch(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      );
  },

  put: (endpoint, params) => {
    return instance
      .put(endpoint, params, {
        validateStatus: (status) => validateStatus(status),
      })
      .then(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      )
      .catch(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      );
  },

  patch: (endpoint, params) => {
    return instance
      .patch(endpoint, params, {
        validateStatus: (status) => validateStatus(status),
      })
      .then(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      )
      .catch(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      );
  },

  delete: (endpoint, params) => {
    return instance
      .delete(endpoint, {
        data: params,
        validateStatus: (status) => validateStatus(status),
      })
      .then(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      )
      .catch(
        (response) => {
          return response;
        },
        (err) => {
          return err.response || err;
        }
      );
  },
});

const api = createApi(createInstance(BASE_URL));

export { api };
