import { Text, View } from "react-native";

export function ExampleDetailScreen() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Example Screen</Text>
    </View>
  );
}
