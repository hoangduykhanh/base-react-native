import { EXAMPLE_SCREEN_DETAIL } from "@navigation/constant/index";
import { GET_LIST_EXAMPLE } from "@redux/constant/example.constant";
import { useEffect } from "react";
import { Button, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";

export function ExampleScreen({ navigation }) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: GET_LIST_EXAMPLE });
  }, []);

  const { data, isLoading } = useSelector((state) => state.exampleReducer);
  console.log("isLoading", isLoading);
  console.log("data", data);

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Example Screen</Text>
      <Button
        title="Go to ExampleDetailScreen"
        onPress={() => navigation.navigate(EXAMPLE_SCREEN_DETAIL)}
      />
    </View>
  );
}
