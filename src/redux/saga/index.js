import { all } from "redux-saga/effects";
import watchGetListExample from "./example/list-example";

function* rootSaga() {
  yield all([watchGetListExample()]);
}

export default rootSaga;
