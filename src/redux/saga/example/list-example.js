import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import {
  GET_LIST_EXAMPLE,
  GET_LIST_EXAMPLE_FAILURE,
  GET_LIST_EXAMPLE_SUCCESS,
} from "../../constant/example.constant.js";

function* doGetListExample() {
  try {
    const result = yield call(async () => {
      return await axios.get(
        "https://fakestoreapi.com/products/category/jewelery"
      );
    });

    if (result && result.success) {
      yield put({ type: GET_LIST_EXAMPLE_SUCCESS, payload: result });
    } else {
      yield put({ type: GET_LIST_EXAMPLE_FAILURE });
    }
  } catch (error) {
    yield put({
      type: GET_LIST_EXAMPLE_FAILURE,
      payload: error.response.message,
    });
  }
}

export default function* watchGetListExample() {
  yield takeLatest(GET_LIST_EXAMPLE, doGetListExample);
}
