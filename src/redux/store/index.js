import { legacy_createStore as createStore, applyMiddleware } from "redux";
import createSagaMiddle from "redux-saga";
import rootReducer from "../reducer";
import rootSaga from "../saga";
import { composeWithDevTools } from "redux-devtools-extension";

const sagaMiddle = createSagaMiddle();

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sagaMiddle))
);

sagaMiddle.run(rootSaga);

export default store;
