import {
  GET_LIST_EXAMPLE,
  GET_LIST_EXAMPLE_FAILURE,
  GET_LIST_EXAMPLE_SUCCESS,
} from "../../constant/example.constant";

const initState = {
  isError: false,
  errorMessage: "",
  data: [],
  isLoading: false,
};

export const exampleReducer = (state = initState, action) => {
  switch (action.type) {
    case GET_LIST_EXAMPLE:
      return {
        ...state,
        isLoading: true,
      };
    case GET_LIST_EXAMPLE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.payload.data,
      };
    case GET_LIST_EXAMPLE_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};
