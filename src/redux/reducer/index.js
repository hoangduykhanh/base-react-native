import { combineReducers } from "redux";
import { exampleReducer } from "./example";

const rootReducer = combineReducers({
  exampleReducer: exampleReducer,
});

export default rootReducer;
